const fs = require('fs');
const { forEach, first, join, toUpper } = require('lodash');
const pluralize = require('pluralize');

const types = [String, Number, Date, Boolean, 'ObjectId', 'Mixed'];

const getTypeName = (name, isRequired, defaultValue) => {
  let res = `${name}`;
  if (defaultValue && ['String', 'Number', 'Boolean'].indexOf(defaultValue.constructor.name) >= 0) res = res + `=${defaultValue}`;
  if (!isRequired) res = `[${res}]`;
  return res;
}

const getTypeString = (name, def, isArray) => {
  if (name === 'Date') name = 'String';
  let res = isArray ? name + '[]' : name;
  if (def.enum) res = res + '="' + join(def.enum, ',') + '"';
  return `{${res}}`;
}

const getTypeDef = (name, obj, isArray, isRequired) => {
  switch (obj.constructor.name) {
    case 'Object':
      let typeName = getTypeName(name, isRequired === undefined ? obj.required : isRequired, obj.default);
      let type = typeof obj.type === 'function' ? getTypeString(obj.type.name, obj, isArray) : getTypeString(obj.type === 'Mixed' ? 'Any' : 'String', {}, isArray);
      let desc = typeof obj.type === 'function' ? `${name} property` : `${name}(${obj.type}) property`;
      return `${type} ${typeName} ${desc}`;
    default:
      return `${getTypeString(obj, {}, isArray)} ${getTypeName(name, isRequired)} ${name} property`;
  }
}

const decodeType = (name, data) => {
  let v = data;
  if (data.constructor.name === 'Schema') {
    v = data.data;
  }
  switch (v.constructor.name) {
    case 'Array':
      return ['[]'].concat(decodeType(name, first(v)));
    case 'Object':
      if (types.indexOf(v.type) >= 0) {
        return [v];
      }
      else {
        let tt = ['Object'];
        forEach(v, (val, key) => {
          tt = tt.concat(`${name}.${key}`, decodeType(key, val));
        });
        return tt;
      }
    default:
      return [`${v}`];
  }
}

class Schema {
  constructor(data) {
    this.data = data;
  }

  getQuery() {
    let result = [];
    forEach(this.data, (value, key) => {
      if (Schema.excludes.indexOf(key) >= 0) return;
      let sub = [' * @apiParam'];
      let decoded = decodeType(key, value);
      if (first(decoded) === '[]') {
        return;
      } else {
        sub.push(getTypeDef(key, 'String', false, false));
      }
      result.push(sub.join(' '));
    });
    return result.join('\n');
  }

  getCreate() {
    let result = [];
    forEach(this.data, (value, key) => {
      if (Schema.excludes.indexOf(key) >= 0) return;
      let sub = [' * @apiParam'];
      let decoded = decodeType(key, value);
      if (first(decoded) === '[]') {
        sub.push(getTypeDef(key, decoded[1], true, undefined));
      } else {
        sub.push(getTypeDef(key, decoded[0], false, undefined));
      }
      result.push(sub.join(' '));
    });
    return result.join('\n');
  }

  getUpdate() {
    let result = [];
    forEach(this.data, (value, key) => {
      if (Schema.excludes.indexOf(key) >= 0) return;
      let sub = [' * @apiParam'];
      let decoded = decodeType(key, value);
      if (first(decoded) === '[]') {
        sub.push(getTypeDef(key, decoded[1], true, false));
      } else {
        sub.push(getTypeDef(key, decoded[0], false, false));
      }
      result.push(sub.join(' '));
    });
    return result.join('\n');
  }

  getSingle() {
    let result = [];
    forEach(this.data, (value, key) => {
      if (Schema.excludes.indexOf(key) >= 0) return;
      let sub = [' * @apiSuccess'];
      let decoded = decodeType(key, value);
      if (first(decoded) === '[]') {
        sub.push(getTypeDef(key, decoded[1], true, true));
      } else {
        sub.push(getTypeDef(key, decoded[0], false, true));
      }
      result.push(sub.join(' '));
      for (let i = 0; i < decoded.length; i++) {
        let entry = String(decoded[i]);
        if (entry.match(/\w{1,}\.\w{1,}/)) {
          let sub2 = [' * @apiSuccess'];
          if (decoded[i + 1] === '[]') {
            sub2.push(getTypeDef(entry, decoded[i + 2], true, true));
          } else {
            sub2.push(getTypeDef(entry, decoded[i + 1], false, true));
          }
          result.push(sub2.join(' '));
        }
      }
    });
    return result.join('\n');
  }

  getMany(name) {
    let result = [];
    forEach(this.data, (value, key) => {
      if (Schema.excludes.indexOf(key) >= 0) return;
      let sub = [' * @apiSuccess'];
      key = name + '.' + key;
      let decoded = decodeType(key, value);
      if (first(decoded) === '[]') {
        sub.push(getTypeDef(key, decoded[1], true, true));
      } else {
        sub.push(getTypeDef(key, decoded[0], false, true));
      }
      result.push(sub.join(' '));
      for (let i = 0; i < decoded.length; i++) {
        let entry = String(decoded[i]);
        if (entry.match(/\w{1,}\.\w{1,}/)) {
          let sub2 = [' * @apiSuccess'];
          if (decoded[i + 1] === '[]') {
            sub2.push(getTypeDef(entry, decoded[i + 2], true, true));
          } else {
            sub2.push(getTypeDef(entry, decoded[i + 1], false, true));
          }
          result.push(sub2.join(' '));
        }
      }
    });
    return result.join('\n');
  }

  index(data) { }
  path(data) {
    return {
      validate: (d) => { }
    }
  }
}

const schemaTypes = {
  ObjectId: 'ObjectId',
  Mixed: 'Mixed',
};

Schema.Types = schemaTypes;

/**
 * 
 * @param {String} string 
 */
let firstUpCase = (string) => {
  return toUpper(string[0]) + string.substr(1)
}

let task = async (modelsFolder, outputFile, excludes) => {
  let files = fs.readdirSync(modelsFolder);
  Schema.excludes = excludes;

  let output = [];
  for (let file of files) {
    if (file === 'index.js') continue;
    let name = file.split('.')[0];
    /**
     * @type Schema
     */
    let model = require(`${modelsFolder}/${file}`)(Schema);
    output.push('/**');
    output.push(` * @apiDefine ModelQuery${firstUpCase(name)}`);
    output.push(model.getQuery());
    output.push(' */\n');
    output.push('/**');
    output.push(` * @apiDefine ModelCreate${firstUpCase(name)}`);
    output.push(model.getCreate());
    output.push(' */\n');
    output.push('/**');
    output.push(` * @apiDefine ModelUpdate${firstUpCase(name)}`);
    output.push(model.getUpdate());
    output.push(' */\n');
    output.push('/**');
    output.push(` * @apiDefine Model${firstUpCase(name)}`);
    output.push(model.getSingle());
    output.push(' */\n');
    output.push('/**');
    output.push(` * @apiDefine Model${firstUpCase(pluralize.plural(name))}`);
    output.push(` * @apiSuccess {Object[]} ${name} List of ${name}`);
    output.push(model.getMany(name));
    output.push(' */\n');
  }

  fs.writeFileSync(outputFile, output.join('\n'));
  console.log(output.join('\n'));
}

module.exports = {
  task
}
