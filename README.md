### installation
```
npm i -g git+https://sourcecontrol4obi@bitbucket.org/sourcecontrol4obi/s4o-mongoose-apidoc.git
```

### usage
```shell
s4o-mongoose-apidoc -i [models-folder] -o [output-file-path]
```

uses [apidocjs](http://apidocjs.com) for model documentation generation

### disclaimer
this tool is still under development, user's descretion is adviced