#!/usr/bin/env node

const lib = require('../tasks');
const path = require('path');

let modelsFolder;
let outputFile;
let exclude;
let version;

for (let i = 0; i < process.argv.length; i++) {
  let current = process.argv[i];
  if (current.match('-i')) modelsFolder = path.resolve(process.argv[i + 1]);
  if (current.match('-o')) outputFile = path.resolve(process.argv[i + 1]);
  if (current.match('-e')) exclude = process.argv[i + 1];
  if (current.match('-version')) version = process.argv[i + 1];
}

if (modelsFolder === undefined || outputFile === undefined) {
  return console.error('incomplete arguments', '-i ctrlFolder -o outputFile');
}

switch (version) {
  case '2':
    lib.task2(modelsFolder, outputFile, exclude.split(',').map((v) => v.trim()));
    break;
  default:
    lib.task1(modelsFolder, outputFile, exclude.split(',').map((v) => v.trim()));
}
