const fs = require('fs');
const {
  forEach,
  first,
  join,
  toUpper,
  uniqBy,
  omit,
  keys,
} = require('lodash');

const types = [String, Number, Date, Boolean, 'ObjectId', 'Mixed'];
const childModels = [];

const getTypeName = (name, isRequired, defaultValue) => {
  let res = `${name}`;
  if (defaultValue && ['String', 'Number', 'Boolean'].indexOf(defaultValue.constructor.name) >= 0) res = res + `=${defaultValue}`;
  if (!isRequired) res = `[${res}]`;
  return res;
}

const getTypeString = (name, def, isArray) => {
  if (name === 'Date') name = 'String';
  let res = isArray ? name + '[]' : name;
  if (def.enum) res = res + '="' + join(def.enum, ',') + '"';
  return `{${res}}`;
}

const getTypeDef = (name, obj, isArray, isRequired) => {
  switch (obj.constructor.name) {
    case 'Object':
      let typeName = getTypeName(name, isRequired === undefined ? obj.required : isRequired, obj.default);
      let type = typeof obj.type === 'function' ? getTypeString(obj.type.name, obj, isArray) : getTypeString(obj.type === 'Mixed' ? 'Any' : 'String', {}, isArray);
      let desc = typeof obj.type === 'function' ? `${name} property` : `${name}(${obj.type}) property`;
      return `${type} ${typeName} ${desc}`;
    default:
      return `${getTypeString(obj, {}, isArray)} ${getTypeName(name, isRequired)} ${name} property`;
  }
}

const decodeType = (name, data, parentName) => {  
  let v = data;
  if (data.constructor.name === 'Schema') {
    v = data.data;
  }
  switch (v.constructor.name) {
    case 'Array':
      return ['[]'].concat(decodeType(name, first(v), parentName));
    case 'Object':
      if (types.indexOf(v.type) >= 0) {
        return [v];
      }
      else {
        const model = `${parentName}_${name}`;
        let tt = [`#${model}`];
        // forEach(v, (val, key) => {
        //   tt = tt.concat(`${name}.${key}`, decodeType(key, val));
        // });
        childModels.push({
          model,
          data,
        })
        return tt;
      }
    default:
      return [`${v}`];
  }
}

const processDocsRemove = (ops, model) => {
  const omitted = [];
  const modelKeys = keys(model);
  modelKeys.forEach((k) => {
    if (model[k]._docsRemove && model[k]._docsRemove.indexOf(ops) >= 0) {
      omitted.push(k);
    }
  });
  return omit(model, omitted);
};

class Schema {
  constructor(data) {
    this.data = data;
  }

  getQuery(name) {
    let result = [];
    forEach(processDocsRemove('read', this.data), (value, key) => {
      if (Schema.excludes.indexOf(key) >= 0) return;
      let sub = [' * @apiParam'];
      let decoded = decodeType(key, value, name);
      if (first(decoded) === '[]') {
        return;
      } else {
        sub.push(getTypeDef(key, 'String', false, false));
      }
      result.push(sub.join(' '));
    });
    return result.join('\n');
  }

  getCreate(name) {
    let result = [];
    forEach(processDocsRemove('create', this.data), (value, key) => {
      if (Schema.excludes.indexOf(key) >= 0) return;
      let sub = [' * @apiParam'];
      let decoded = decodeType(key, value, name);
      if (first(decoded) === '[]') {
        sub.push(getTypeDef(key, decoded[1], true, undefined));
      } else {
        sub.push(getTypeDef(key, decoded[0], false, undefined));
      }
      result.push(sub.join(' '));
    });
    return result.join('\n');
  }

  getUpdate(name) {
    let result = [];
    forEach(processDocsRemove('update', this.data), (value, key) => {
      if (Schema.excludes.indexOf(key) >= 0) return;
      let sub = [' * @apiParam'];
      let decoded = decodeType(key, value, name);
      if (first(decoded) === '[]') {
        sub.push(getTypeDef(key, decoded[1], true, false));
      } else {
        sub.push(getTypeDef(key, decoded[0], false, false));
      }
      result.push(sub.join(' '));
    });
    return result.join('\n');
  }

  getSingle(name) {
    let result = [];
    forEach(processDocsRemove('read', this.data), (value, key) => {
      if (Schema.excludes.indexOf(key) >= 0) return;
      let sub = [' * @apiSuccess'];
      let decoded = decodeType(key, value, name);
      if (first(decoded) === '[]') {
        sub.push(getTypeDef(key, decoded[1], true, false));
      } else {
        sub.push(getTypeDef(key, decoded[0], false, false));
      }
      result.push(sub.join(' '));
      for (let i = 0; i < decoded.length; i++) {
        let entry = String(decoded[i]);
        if (entry.match(/\w{1,}\.\w{1,}/)) {
          let sub2 = [' * @apiSuccess'];
          if (decoded[i + 1] === '[]') {
            sub2.push(getTypeDef(entry, decoded[i + 2], true, false));
          } else {
            sub2.push(getTypeDef(entry, decoded[i + 1], false, false));
          }
          result.push(sub2.join(' '));
        }
      }
    });
    return result.join('\n');
  }

  getMany(name) {
    let result = [];
    forEach(processDocsRemove('read', this.data), (value, key) => {
      if (Schema.excludes.indexOf(key) >= 0) return;
      let sub = [' * @apiSuccess'];
      key = name + '.' + key;
      let decoded = decodeType(key, value);
      if (first(decoded) === '[]') {
        sub.push(getTypeDef(key, decoded[1], true, true));
      } else {
        sub.push(getTypeDef(key, decoded[0], false, true));
      }
      result.push(sub.join(' '));
      for (let i = 0; i < decoded.length; i++) {
        let entry = String(decoded[i]);
        if (entry.match(/\w{1,}\.\w{1,}/)) {
          let sub2 = [' * @apiSuccess'];
          if (decoded[i + 1] === '[]') {
            sub2.push(getTypeDef(entry, decoded[i + 2], true, true));
          } else {
            sub2.push(getTypeDef(entry, decoded[i + 1], false, true));
          }
          result.push(sub2.join(' '));
        }
      }
    });
    return result.join('\n');
  }

  index(data) { }
  path(data) {
    return {
      validate: (d) => { }
    }
  }
}

const schemaTypes = {
  ObjectId: 'ObjectId',
  Mixed: 'Mixed',
};

Schema.Types = schemaTypes;

/**
 * 
 * @param {String} string 
 */
let firstUpCase = (string) => {
  return toUpper(string[0]) + string.substr(1)
}

let generateMain = (output, name, model) => {
  output.push('/**');
  output.push(` * @apiDefine ModelQuery${firstUpCase(name)}`);
  output.push(model.getQuery(name));
  output.push(' */\n');
  output.push('/**');
  output.push(` * @apiDefine ModelCreate${firstUpCase(name)}`);
  output.push(model.getCreate(name));
  output.push(' */\n');
  output.push('/**');
  output.push(` * @apiDefine ModelUpdate${firstUpCase(name)}`);
  output.push(model.getUpdate(name));
  output.push(' */\n');
  output.push('/**');
  output.push(` * @apiDefine Model${firstUpCase(name)}`);
  output.push(model.getSingle(name));
  output.push(' */\n');
};

let generateChild = (output, name, model) => {
  output.push('/**');
  output.push(` * @apiDefine ${name}`);
  output.push(model.getCreate(name));
  output.push(' */\n');
};

let task = async (modelsFolder, outputFile, excludes) => {
  let files = fs.readdirSync(modelsFolder);
  Schema.excludes = excludes;

  let output = [];
  const generated = [];
  for (let file of files) {
    if (file === 'index.js') continue;
    let name = file.split('.')[0];
    /**
     * @type Schema
     */
    let model = require(`${modelsFolder}/${file}`)(Schema);
    generateMain(output, name, model);

    let cModels = uniqBy(childModels, 'model');
    cModels.forEach((m) => {
      if (generated.indexOf(m.model) < 0) {
        generateChild(output, m.model, new Schema(m.data));
        generated.push(m.model);
      }
    });
  }

  fs.writeFileSync(outputFile, output.join('\n'));
  console.log(output.join('\n'));
}

module.exports = {
  task
}
